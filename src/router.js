import { createRouter, createWebHashHistory } from 'vue-router'
import AboutPage from "./pages/AboutPage.vue"
import LoginForm from "./components/LoginForm.vue"
import HomePage from "./pages/HomePage.vue"
import Product from "./pages/Product.vue"

export const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/about',
            name: 'about',
            component: AboutPage
        },
        {
            path:'/login',
            name : 'login',
            component : LoginForm
        },
        {
            path: '',
            alias: '/home',
            name: 'home',
            component: HomePage
        },
        {
            path: '/product/:id',
            name: 'product',
            component : Product
        },
    ]
});


